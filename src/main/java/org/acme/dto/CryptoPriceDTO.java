package org.acme.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.util.Date;

@Jacksonized
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CryptoPriceDTO {

    public String id;
    public String symbol;
    public String name;
    public String image;
    public int current_price;
    public long market_cap;
    public int market_cap_rank;
    public long fully_diluted_valuation;
    public long total_volume;
    public int high_24h;
    public int low_24h;
    public double price_change_24h;
    public double price_change_percentage_24h;
    public long market_cap_change_24h;
    public double market_cap_change_percentage_24h;
    public int circulating_supply;
    public int total_supply;
    public int max_supply;
    public int ath;
    public double ath_change_percentage;
    public Date ath_date;
    public double atl;
    public double atl_change_percentage;
    public Date atl_date;
    public Object roi;
    public Date last_updated;

}
